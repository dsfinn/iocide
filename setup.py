from setuptools import setup

from roboversion import get_version


with open('README.md') as readme:
	long_description = readme.read()


package_data = {
	'name': 'iocide',
	'version': str(get_version()),
	'packages': ['iocide'],
	'author': 'David Finn',
	'author_email': 'dsfinn@gmail.com',
	'classifiers': [
		'License :: OSI Approved :: GNU General Public License v3 (GPLv3)',
		'Programming Language :: Python :: 3.6',
		'Topic :: Security',
	],
	'description': 'Indicator of Compromise (IOC) Detection Utility',
	'entry_points': {'console_scripts': ['iocide = iocide.cli:main']},
	'install_requires': [
		'chardet>=4.0.0',
		'pdfminer.six>=20201018',
		'regex>=2021.4.4',
		'unidecode>=1.2.0',
	],
	'license_files': ['LICENSE'],
	'long_description': long_description,
	'long_description_content_type': "text/markdown",
	'package_data': {'iocide': ['data/*']},
	'python_requires': '>=3.6',
	'url': 'https://gitlab.com/dsfinn/iocide',
}


if __name__ == '__main__':
	setup(**package_data)
