import base64
from functools import partial
from iocide.email import ParsedAddress
from ipaddress import IPv6Address
from string import ascii_letters, ascii_lowercase, digits, hexdigits
from urllib.parse import ParseResult, quote

from hypothesis import assume, example, given, seed
from hypothesis.strategies import (
	composite,
	integers,
	ip_addresses,
	iterables,
	none,
	sampled_from,
	sets,
	text,
)
from hypothesis.strategies._internal.strategies import Ex
import ioc_fanger
from pytest import raises

import iocide
from iocide.hostname import TLD_LIST
import iocide.rfc_3548
from iocide.url import URI_SUB_DELIMS, URI_UNENCODED_PCHAR
from iocide.inputs import EncodingDetectionFailure


@composite
def url_encoded_text(draw, whitelist=None, max_size=None, **kwargs):
	whitelist = whitelist if whitelist else '' 
	size = 0
	characters = []
	for character in draw(text(max_size=max_size, **kwargs)):
		if character not in whitelist:
			character = quote(character)
			if character == '/':
				character = r'%2F'
		
		if max_size is None:
			characters.append(character)
			continue

		character_len = len(character)
		if size + character_len > max_size:
			break

		characters.append(character)
		size += character_len

	return ''.join(characters)

@composite
def url_schemes(draw):
	start = draw(sampled_from(ascii_lowercase))
	following = draw(
		text(alphabet=(digits + ascii_lowercase + '+.-'), max_size=64))
	return start + following


@composite
def user_information(draw):
	username = draw(
		url_encoded_text(whitelist=URI_SUB_DELIMS, min_size=1, max_size=64))
	password = draw(
		(
			none()
			| url_encoded_text(whitelist=(URI_SUB_DELIMS+':'), max_size=64)
		),
	)
	return username if (password is None) else f'{username}:{password}'


@composite
def hostname_segments(draw):
	start = draw(sampled_from(digits + ascii_letters))
	middle = draw(text(alphabet=(digits + ascii_letters + '-'), max_size=61))
	end = draw(sampled_from(digits + ascii_letters))
	return start + middle + end

HOSTNAME_SIZE_LIMIT = 64

@composite
def hostnames(draw):
	tld = draw(sampled_from(TLD_LIST))

	while True:
		segment_iterable = draw(
			iterables(elements=hostname_segments(), min_size=1, max_size=16))
		segments = [next(segment_iterable), tld]
		if len('.'.join(segments)) < HOSTNAME_SIZE_LIMIT:
			break

	for segment in segment_iterable:
		segments.insert(0, segment)
		if len('.'.join(segments)) >= HOSTNAME_SIZE_LIMIT:
			segments = segments[1:]
			break

	return '.'.join(segments)


@composite
def remote_ip_addresses(draw, **kwargs):
	return draw(ip_addresses(**kwargs).filter(lambda a: a != IPv6Address(0)))

@composite
def ipv6_hosts(draw):
	address = draw(remote_ip_addresses(v=6))
	return f'[{address}]'

@composite
def netlocs(draw):
	value = str(draw(remote_ip_addresses(v=4) | ipv6_hosts() | hostnames()))
	port = draw(none() | integers(min_value=0, max_value=65535))
	userinfo = draw(none() | user_information())
	if userinfo is not None:
		value = f'{userinfo}@{value}'

	if port is not None:
		value += f':{port}'

	return value

@composite
def paths(draw):
	segments = ['']
	segments.extend(
		draw(
			iterables(
				url_encoded_text(whitelist=URI_UNENCODED_PCHAR, max_size=64),
				max_size=64,
			),
		),
	)
	return '/'.join(segments)

@composite
def queries(draw):
	allowed_unencoded= URI_UNENCODED_PCHAR + ':@/?'
	return draw(url_encoded_text(max_size=128, whitelist=allowed_unencoded))

@composite
def urls(draw):
	return iocide.url.ParsedUrl(
		scheme=draw(url_schemes()),
		netloc=draw(netlocs()),
		path=draw(paths()),
		query=draw(queries()),
		fragment=draw(url_encoded_text(max_size=64)),
	)

HASH_SIZES = [32, 40, *(int(s/4) for s in iocide.hashes.SHA2_SIZES)]

@composite
def hashes(draw):
	size = draw(sampled_from(HASH_SIZES))
	return draw(text(alphabet=hexdigits, min_size=size, max_size=size))

@composite
def unquoted_email_local(draw):
	value = '.'.join(
		draw(
			iterables(
				text(
					alphabet=iocide.email.unquoted_local_alphabet, min_size=1),
				min_size=1,
			)
		)
	)
	assume(len(value) <= 64)
	return value

@composite
def quoted_email_local(draw):
	quoted_value = draw(
		text(alphabet=iocide.email.quoted_alphabet), min_size=1, max_size=62)

	return f'"{quoted_value}"'

@composite
def email_domain_ips(draw):
	return f'[{draw(remote_ip_addresses())}]'

@composite
def valid_tld_emails(draw):
	local = draw(unquoted_email_local() or quoted_email_local())
	domain = draw(hostnames() | email_domain_ips())
	return iocide.email.ParsedAddress(username=local, domain=domain)

@composite
def rfc_3548_schemes(draw):
	return draw(sampled_from(iocide.rfc_3548.SCHEMES))

@composite
def rfc_3548_encoded_text(draw, text_encoding, **kwargs):
	encode = draw(
		sampled_from([base64.b64encode, base64.b32encode, base64.b16encode]))
	raw_text = draw(text(**kwargs))
	text_bytes = raw_text.encode(text_encoding)
	encoded_bytes = encode(text_bytes)
	return raw_text, encoded_bytes.decode('ascii')

@composite
def rfc_3548_schemes(draw):
	return draw(sampled_from(list(iocide.rfc_3548.SCHEMES)))

@composite
def text_encodings(draw):
	return draw(sampled_from(['utf_8']))


@composite
def iocs(draw):
	return draw(
		urls()
		| valid_tld_emails()
		| hostnames()
		| remote_ip_addresses()
		| hashes()
	)

@composite
def ioc_text(draw, min_size=0, max_size=None):
	length = draw(integers(min_value=min_size, max_value=max_size))
	ioc_iterable = draw(sets(iocs(), min_size=1))
	current_length = 0
	segments = []
	ioc_set = set()
	for ioc in ioc_iterable:
		ioc = draw(iocs())
		ioc_text = f'{ioc} '
		ioc_length = len(ioc_text)
		new_length = current_length + ioc_length
		if new_length > length:
			break

		segments.append(ioc_text)
		ioc_set.add(ioc)
		current_length = new_length

	missing_length = length - current_length
	if missing_length > 0:
		filler = '%' * missing_length
		segments.append(filler)

	return ioc_set, ''.join(segments)


RFC_3548_ENCODERS = {
	'base32': base64.b32encode,
	'base16': base64.b16encode,
	'base64': base64.b64encode,
	'base64_url_filename': partial(base64.b64encode, altchars=b'-_'),
}


@composite
def encoded_ioc_text(draw, min_size=None, max_size=None):
	min_min = iocide.rfc_3548.MIN_TEXT_LENGTH
	max_max = iocide.rfc_3548.MAX_TEXT_LENGTH
	if min_size is None:
		min_size = min_min

	if max_size is None:
		max_size = max_max

	if min_size < min_min:
		raise ValueError(f'min_size must not subceed {min_min}')

	if max_size > max_max:
		raise ValueError(f'max_size must not exceed {max_max}')

	ioc_set, raw_text = draw(ioc_text(min_size=min_size, max_size=max_size))
	scheme = draw(rfc_3548_schemes())
	encoding = draw(text_encodings())
	data = raw_text.encode(encoding)
	encoded_blob = RFC_3548_ENCODERS[scheme](data).decode('ascii')
	assume(not iocide.hashes.PATTERN_TEMPLATE.regex.fullmatch(encoded_blob))
	return ioc_set, encoded_blob


@given(ip_address=remote_ip_addresses())
def test_ips(ip_address):
	found_ip, = iocide.ip.extract(text=str(ip_address))
	assert found_ip == ip_address
	defanged = ioc_fanger.defang(str(ip_address))
	found_defanged, = iocide.ip.extract(text=str(defanged))
	assert found_defanged == ip_address
	refanged = iocide.ip.refang(str(found_defanged))
	assert refanged == ip_address


@example(hostname='00.dot')
@given(hostname=hostnames())
def test_hostnames(hostname):
	found_hostname, = iocide.hostname.extract(text=hostname)
	assert found_hostname == hostname
	defanged = ioc_fanger.defang(hostname)
	found_defanged, = iocide.hostname.extract(text=defanged)
	assert found_defanged == defanged
	refanged = iocide.hostname.refang(found_defanged)
	assert refanged == hostname

@example(
	url=iocide.url.ParsedUrl(
		scheme='n1',
		netloc='%C3%94:%5E@01.aarp',
		path='',
		query='%F0%90%B3%A31',
		fragment='',
	)
)
@given(url=urls())
def test_urls(url):
	url_string = iocide.url.ParsedUrl.get_string(url)
	found_url, = iocide.url.extract(text=url_string, refang=False)
	assert found_url == url
	assert str(found_url) == url_string
	defanged = ioc_fanger.defang(url_string)
	found_defanged, = iocide.url.extract(text=defanged)
	found_defanged_string = str(found_defanged)
	assert found_defanged_string == defanged
	refanged = iocide.url.refang(found_defanged_string)
	assert _is_roundtripped(refanged, url)



@given(hash=hashes())
def test_hashes(hash):
	found_hash, = iocide.hashes.extract(text=hash)
	assert found_hash == hash


@example(email=ParsedAddress('"quoted\nnewline"', 'domain.com'))
@given(email=valid_tld_emails())
def test_emails(email):
	email_text = str(email)
	found_email, = iocide.email.extract(text=email_text)
	assert found_email == email
	defanged_text = ioc_fanger.defang(email_text)
	defanged = iocide.email.ParsedAddress(
	username=email.username, domain=email.domain, defanged=defanged_text)

	found_defanged, = iocide.email.extract(text=defanged_text)
	found_defanged_text = str(found_defanged)
	assert found_defanged == defanged
	assert found_defanged_text == defanged_text
	refanged = iocide.email.refang(found_defanged_text)
	assert refanged == email



@example(
	text='00000000000000000000000000000Ā',
	scheme='base32',
	encoding='utf_8',
)
@given(
	text=text(min_size=iocide.rfc_3548.MIN_TEXT_LENGTH),
	scheme=rfc_3548_schemes(),
	encoding=text_encodings(),
)
def test_rfc_3548(text, scheme, encoding):
	text_bytes = text.encode(encoding)
	encoder = RFC_3548_ENCODERS[scheme]
	encoded_bytes = encoder(text_bytes)
	encoded_text = encoded_bytes.decode(encoding)
	assume(not iocide.hashes.PATTERN_TEMPLATE.regex.fullmatch(encoded_text))

	found_data, = iocide.blobs.extract(text=encoded_text, refang=True)
	assert found_data == text_bytes
	known_encoding_result = found_data.decode(encoding)
	assert known_encoding_result == text

	found_text, = iocide.blobs.extract_text(data=found_data)
	try:
		assert found_text == text
	except AssertionError:
		with raises(EncodingDetectionFailure) as exc_info:
			next(
				iocide.blobs.extract_text(
					data=found_data,
					required_confidence=1,
					skip_failures=False,
					fallback=None,
				)
			)

		detected_encoding = exc_info.value.encoding
		assert detected_encoding != encoding


@example(
	embedded_iocs=(
		{'0000000000000000000000000000000000000000'},
		'GAYDAMBQGAYDAMBQGAYDAMBQGAYDAMBQGAYDAMBQGAYDAMBQGAYDAMBQGAYDAMBQEA======'
	),
)
@seed(44503986010879582645142168078789020727)
@given(
	embedded_iocs=(
		ioc_text(max_size=0x1000)
		| encoded_ioc_text()
	),
)
def test_extract_all(embedded_iocs):
	ioc_set, ioc_text = embedded_iocs
	ioc_data = ioc_text.encode()
	found_iocs = set()
	for found_text in iocide.blobs.extract_text(ioc_data, depth=None):
		found_iocs.update(iocide.extract_all(text=found_text))

	assert ioc_set.issubset(found_iocs)

	defanged_text = ioc_fanger.defang(ioc_text)
	defanged_data = defanged_text.encode()
	found_defanged_iocs = set()
	for found_text in iocide.blobs.extract_text(defanged_data, depth=None):
		found_defanged_iocs.update(
			iocide.extract_all(text=found_text, refang=True))

	remaining_iocs = set(found_defanged_iocs)
	for ioc in ioc_set:
		for found_ioc in remaining_iocs:
			if _is_roundtripped(found_ioc, ioc):
				break
		else:
			raise ValueError(f'Did not find defanged {ioc!r}')

		remaining_iocs.remove(found_ioc)


def _is_roundtripped(ioc, comparator):
	if not all(isinstance(x, ParseResult) for x in [ioc, comparator]):
		return str(ioc) == str(comparator)

	for refangable in ['scheme', 'netloc']:
		if getattr(ioc, refangable) != getattr(comparator, refangable):
			return False

	return True
